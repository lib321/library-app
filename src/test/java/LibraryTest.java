import org.example.Book;
import org.example.Library;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LibraryTest {

    @Test
    public void testAddBook() {
        Library library = new Library();
        Book book = new Book("Title1", "Author1", 2000, "ISBN1");
        library.addBook(book);

        List<Book> books = library.getAllBooks();
        assertEquals(1, books.size());
        assertEquals("Title1", books.get(0).getName());
    }

    @Test
    public void testDeleteBook() {
        Library library = new Library();
        Book book1 = new Book("Title1", "Author1", 2000, "ISBN1");
        Book book2 = new Book("Title2", "Author2", 2001, "ISBN2");
        library.addBook(book1);
        library.addBook(book2);

        library.deleteBook("ISBN1");

        List<Book> books = library.getAllBooks();
        assertEquals(1, books.size());
        assertEquals("Title2", books.get(0).getName());
    }

    @Test
    public void testSearchByName() {
        Library library = new Library();
        Book book1 = new Book("Title1", "Author1", 2000, "ISBN1");
        Book book2 = new Book("Title2", "Author2", 2001, "ISBN2");
        library.addBook(book1);
        library.addBook(book2);

        List<Book> results = library.searchByName("Title1");
        assertEquals(1, results.size());
        assertEquals("Title1", results.get(0).getName());
    }

    @Test
    public void testSearchByAuthor() {
        Library library = new Library();
        Book book1 = new Book("Title1", "Author1", 2000, "ISBN1");
        Book book2 = new Book("Title2", "Author2", 2001, "ISBN2");
        library.addBook(book1);
        library.addBook(book2);

        List<Book> results = library.searchByAuthor("Author2");
        assertEquals(1, results.size());
        assertEquals("Title2", results.get(0).getName());
    }

    @Test
    public void testSearchByYear() {
        Library library = new Library();
        Book book1 = new Book("Title1", "Author1", 2000, "ISBN1");
        Book book2 = new Book("Title2", "Author2", 2001, "ISBN2");
        library.addBook(book1);
        library.addBook(book2);

        List<Book> results = library.searchByYear(2001);
        assertEquals(1, results.size());
        assertEquals("Title2", results.get(0).getName());
    }

    @Test
    public void testGetAllBooks() {
        Library library = new Library();
        Book book1 = new Book("Title1", "Author1", 2000, "ISBN1");
        Book book2 = new Book("Title2", "Author2", 2001, "ISBN2");
        library.addBook(book1);
        library.addBook(book2);

        List<Book> results = library.getAllBooks();
        assertEquals(2, results.size());
        assertEquals("Title1", results.get(0).getName());
        assertEquals("Title2", results.get(1).getName());
    }
}
