package org.example;

import java.util.List;
import java.util.Scanner;

public class LibraryApp {

    private static Library library = new Library();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("1. Добавить книгу");
            System.out.println("2. Удалить книгу");
            System.out.println("3. Найти книгу");
            System.out.println("4. Показать все книги");
            System.out.println("5. Выйти");

            int choice = Integer.parseInt(scanner.nextLine());

            switch (choice) {
                case 1:
                    addBook();
                    break;
                case 2:
                    removeBook();
                    break;
                case 3:
                    searchBooks();
                    break;
                case 4:
                    showAllBooks();
                    break;
                case 5:
                    System.exit(0);
            }
        }
    }

    private static void addBook() {
        System.out.print("Введите название: ");
        String name = scanner.nextLine();
        System.out.print("Введите автора: ");
        String author = scanner.nextLine();
        System.out.print("Введите год издания: ");
        int year = Integer.parseInt(scanner.nextLine());
        System.out.print("Введите ISBN: ");
        String isbn = scanner.nextLine();

        Book book = new Book(name, author, year, isbn);
        library.addBook(book);
        System.out.println("Книга добавлена.");
    }

    private static void removeBook() {
        System.out.print("Введите ISBN книги для удаления: ");
        String isbn = scanner.nextLine();
        library.deleteBook(isbn);
        System.out.println("Книга удалена.");
    }

    private static void searchBooks() {
        System.out.println("1. Поиск по названию");
        System.out.println("2. Поиск по автору");
        System.out.println("3. Поиск по году издания");

        int choice = Integer.parseInt(scanner.nextLine());
        List<Book> results = null;

        switch (choice) {
            case 1:
                System.out.print("Введите название: ");
                String name = scanner.nextLine();
                results = library.searchByName(name);
                break;
            case 2:
                System.out.print("Введите автора: ");
                String author = scanner.nextLine();
                results = library.searchByAuthor(author);
                break;
            case 3:
                System.out.print("Введите год издания: ");
                int year = Integer.parseInt(scanner.nextLine());
                results = library.searchByYear(year);
                break;
        }

        if (results != null && !results.isEmpty()) {
            results.forEach(System.out::println);
        } else {
            System.out.println("Книги не найдены.");
        }
    }

    private static void showAllBooks() {
        List<Book> books = library.getAllBooks();
        if (!books.isEmpty()) {
            books.forEach(System.out::println);
        } else {
            System.out.println("В библиотеке нет книг.");
        }
    }
}
